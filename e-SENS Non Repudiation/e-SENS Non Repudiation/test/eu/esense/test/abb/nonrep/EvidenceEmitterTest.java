package eu.esense.test.abb.nonrep;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.LinkedList;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.apache.log4j.BasicConfigurator;
import org.herasaf.xacml.core.api.PDP;
import org.herasaf.xacml.core.api.UnorderedPolicyRepository;
import org.herasaf.xacml.core.policy.PolicyMarshaller;
import org.herasaf.xacml.core.simplePDP.SimplePDPFactory;
import org.joda.time.DateTime;
import org.junit.BeforeClass;
import org.junit.Test;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.xml.sax.SAXException;

import eu.esens.abb.nonrep.ESensObligationHandler;
import eu.esens.abb.nonrep.EnforcePolicy;
import eu.esens.abb.nonrep.EnforcePolicyException;
import eu.esens.abb.nonrep.IHEXCARetrieve;
import eu.esens.abb.nonrep.MalformedIHESOAPException;
import eu.esens.abb.nonrep.MessageInspector;
import eu.esens.abb.nonrep.MessageType;
import eu.esens.abb.nonrep.ObligationDischargeException;
import eu.esens.abb.nonrep.ObligationHandler;
import eu.esens.abb.nonrep.ObligationHandlerFactory;
import eu.esens.abb.nonrep.TOElementException;
import eu.esens.abb.nonrep.Utilities;
import eu.esens.abb.nonrep.XACMLAttributes;
import eu.esens.abb.nonrep.XACMLRequestCreator;

public class EvidenceEmitterTest {

	public static final String DATATYPE_STRING = "http://www.w3.org/2001/XMLSchema#string";
	public static final String DATATYPE_DATETIME = "http://www.w3.org/2001/XMLSchema#dateTime";
	public static final String IHE_ITI_XCA_RETRIEVE = "urn:ihe:iti:2007:CrossGatewayRetrieve";
	private static PDP simplePDP;

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		
		/*
		 * the polrep is here because an adHoc implementation can have more
		 * requirements than the default from herasaf, if needed
		 */
		simplePDP = SimplePDPFactory.getSimplePDP();
		UnorderedPolicyRepository polrep = (UnorderedPolicyRepository)simplePDP.getPolicyRepository();
		
		// Populate the policy repository
		Document policy = readMessage("test/testData/samplePolicy.xml");
		
		polrep.deploy(PolicyMarshaller.unmarshal(policy));
	}

	/**
	 * This test reads a sample message from the eHealth domain (XCA) and will
	 * issue an ATNA-specific audit trail.
	 * 
	 * @throws ParserConfigurationException
	 * @throws IOException
	 * @throws SAXException
	 * @throws MalformedIHESOAPException
	 * @throws URISyntaxException 
	 * @throws TOElementException 
	 * @throws EnforcePolicyException 
	 * @throws ObligationDischargeException 
	 */
	@Test
	public void testGenerateATNA() throws ParserConfigurationException,
			SAXException, IOException, MalformedIHESOAPException, URISyntaxException, TOElementException, EnforcePolicyException, ObligationDischargeException {
		testGenerateAtna();
	}

	/**
	 * I had to add this method because I need the message, but Junit does not like non-void test methods.
	 * @return
	 * @throws ParserConfigurationException
	 * @throws SAXException
	 * @throws IOException
	 * @throws MalformedIHESOAPException
	 * @throws URISyntaxException
	 * @throws TOElementException
	 * @throws EnforcePolicyException
	 * @throws ObligationDischargeException
	 */
  public Document testGenerateAtna() throws ParserConfigurationException, SAXException, IOException,
      MalformedIHESOAPException, URISyntaxException, TOElementException, EnforcePolicyException,
      ObligationDischargeException {
    /*
		 * AlternativeUserID = IP of the machine NetworkAccessPointID = IP of
		 * the machine UserName = user from the SAML assertion
		 * 
		 * NetworkAccessPointID = IP of the remote
		 * 
		 * UserId = Subject of the assertion UserName = subject-id
		 * 
		 * ParticipantObjectID = xdsb:RepositoryUniqueId ParticipantObjectID =
		 * xdsb:DocumentUniqueId
		 */

		/*
		 * The flow is as follows (imagine that the PEP is a facade in front of
		 * the Corner). The message is inspected, the relevant information is
		 * retrieved and placed into the XACML request. The PDP evaluates the
		 * request and returns the pointer of the obligation handler.
		 */

		// Configure Log4j
		BasicConfigurator.configure();

		// Read the message as it arrives at the facade
		Document incomingMsg = readMessage("test/testData/incomingMsg.xml");

		/*
		 * Instantiate the message inspector, to see which type of message is
		 */
		MessageInspector messageInspector = new MessageInspector(incomingMsg);
		MessageType messageType = messageInspector.getMessageType();
		assertNotNull(messageType);

		/*
		 * In this mock, we have an IHE
		 */
		checkCorrectnessofIHEXCA(messageType);
		
		/*
		 * Now create the XACML request
		 */
		LinkedList<XACMLAttributes> actionList = new LinkedList<XACMLAttributes>();
		XACMLAttributes action = new XACMLAttributes();
		action.setDataType(new URI(DATATYPE_STRING));
		action.setIdentifier(new URI("urn:oasis:names:tc:xacml:1.0:action:action-id"));
		actionList.add(action);
		
		// Here I imagine a table lookup or similar
		action.setValue(messageType instanceof IHEXCARetrieve?"IHE_ITI_XCA_RETRIEVE":"UNKNOWN");
		
		
		LinkedList<XACMLAttributes> environmentList = new LinkedList<XACMLAttributes>();
		XACMLAttributes environment = new XACMLAttributes();
		environment.setDataType(new URI(DATATYPE_DATETIME));
		environment.setIdentifier(new URI("urn:esens:2014:event"));
		environment.setValue(new DateTime().toString());
		environmentList.add(environment);
		
		XACMLRequestCreator requestCreator = new XACMLRequestCreator(messageType, 
				null, 
				null, 
				actionList,
				environmentList);
		
		Element request=requestCreator.getRequest();
		assertNotNull(request);
		
		// just some printouts
		Utilities.serialize(request);
		
		/*
		 * Call the XACML engine. 
		 * 
		 * The policy has been deployed in the setupBeforeClass.
		 */
		EnforcePolicy enforcePolicy = new EnforcePolicy(simplePDP);
		
		enforcePolicy.decide(request);
		assertNotNull(enforcePolicy.getResponseAsDocument());
		assertNotNull(enforcePolicy.getResponseAsObject());
		Utilities.serialize(enforcePolicy.getResponseAsDocument().getDocumentElement());

		List<ESensObligationHandler> obligations = enforcePolicy.getObligationList();
		assertNotNull(obligations);
		
		ObligationHandlerFactory handlerFactory = ObligationHandlerFactory.getInstance();
		ObligationHandler handler = handlerFactory.createHandler(messageType, obligations);
		handler.discharge();
		
		//this assertion keeps failing.
		assertNotNull(handler.getMessage());
		
		//I think I need to return handler.getMessage() which will be the audit
		//the audit will go to the server and get validated by another wrapper
		return handler.getMessage();
  }

	private static Document readMessage(String file) throws ParserConfigurationException,
			SAXException, IOException {
		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
		dbf.setNamespaceAware(true);
		DocumentBuilder db = dbf.newDocumentBuilder();
		Document incomingMsg = db.parse(new File(file
				));
		return incomingMsg;
	}

	private void checkCorrectnessofIHEXCA(final MessageType messageType) {
		assertTrue(messageType instanceof IHEXCARetrieve);

		IHEXCARetrieve xca = (IHEXCARetrieve) messageType;
		assertNotNull(xca.getDocumentUniqueId());
		assertNotNull(xca.getHomeCommunityID());
		assertNotNull(xca.getRepositoryUniqueId());

		assertEquals("urn:oid:2.16.840.1.113883.3.42.10001.100001.19",
				xca.getHomeCommunityID());
		assertEquals("2.16.840.1.113883.3.333.1", xca.getRepositoryUniqueId());
		assertEquals("4eb38f09-78da-43a8-a5b4-92b115c74add",
				xca.getDocumentUniqueId());
	}
}
